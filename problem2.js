const fs = require("fs");

function fileOperationsPerform(){
fs.readFile("./lipsum.txt", "utf-8", (err, data) => {
  if (err) {
    console.error("Error while reading the file");
  } else {
    createUpperCaseFile(data);
  }
});
}

function createUpperCaseFile(data) {
  let dataInUpperCase = data.toUpperCase();
  console.log("Data converted to Upper Case");
  let newFileName = "upperCaseNewFile1.txt";
  fs.writeFile("./upperCaseNewFile1.txt", dataInUpperCase, (err) => {
    if (err) {
      console.error("Error while creating upperCaseNewFile1.txt file");
    } else {
      console.log("upperCaseNewFile1.txt file created successfully");
      fs.writeFile("./filenames.txt", newFileName, (err) => {
        if (err) {
          console.error(
            "Problem while storing newfilename into the filenames.txt file"
          );
        } else {
          console.log("filename stored in the filenames.txt file");
          createLowerCaseFile(dataInUpperCase);
        }
      });
    }

    function createLowerCaseFile(dataInUpperCase) {
      let dataInLowerCase = dataInUpperCase.toLowerCase();
      fs.writeFile("./lowerCaseNewFile2.txt", dataInLowerCase, (err) => {
        if (err) {
          console.error("Error while creating lowerCaseNewFile2.txt file");
        } else {
          console.log("lowerCaseNewFile2.txt file created successfully");
          fs.readFile("./lowerCaseNewFile2.txt", "utf-8", (err, data) => {
            if (err) {
              console.error(
                "Problem in reading data of file lowerCaseNewFile2.txt"
              );
            } else {
              console.log("lowerCaseNewFile2.txt file readed sucssesfully");
              createFileOfSplitLines(data);
            }
          });
        }
      });
    }
  });
}

function createFileOfSplitLines(data) {
  let dataArray = data.split(". ");
  let newFIleName2 = " splitLinesFile3.txt";
  fs.writeFile("./splitLinesFile3.txt", dataArray, (err) => {
    if (err) {
      console.error("Error while creating file splitLinesFile3.txt");
    } else {
      console.log("splitLinesFile3.txt file created successfully");
      fs.appendFile("./filenames.txt", newFIleName2, (err) => {
        if (err) {
          console.error("fail to append filename in the filenames.txt file");
        } else {
          console.log("splitLinesFile3.txt name append in filenames.txt file");
          fs.readFile("./splitLinesFile3.txt", "utf-8", (err, data) => {
            if (err) {
              console.error("problem in reading file splitLinesFile3.txt");
            } else {
              console.log("splitLinesFile3.txt file reading completed");
              createSortedDataFile(data);
            }
          });
        }
      });
    }
  });
}

function createSortedDataFile(data) {
  let sortedData = data.split(" ").sort();
  let fileName3 = " sortedDataFile4.txt";
  fs.writeFile("./sortedDataFile4.txt", sortedData, (err) => {
    if (err) {
      console.error("Error while creating file sortedDataFile4.txt");
    } else {
      fs.appendFile("./filenames.txt", fileName3, (err) => {
        if (err) {
          console.error("fail to append filename in the filenames.txt file");
        } else {
          console.log("sortedDataFile4.txt name append in filenames.txt file");
          fs.readFile("./filenames.txt", "utf-8", (err, data) => {
            if (err) {
              console.error("Problem in reading file filenames.txt");
            } else {
              console.log("filenames.txt file readed successfully");
              fileDeleter(data);
            }
          });
        }
      });
    }
  });
}

function fileDeleter(data) {
  let files = data.split(" ");
  files.forEach((fileName) => {
    fs.unlink(fileName, (err) => {
      if (err) {
        console.error("Error while deleting files");
      } else {
        console.log("files are deleted successfully");
      }
    });
  });
}

module.exports = fileOperationsPerform;
